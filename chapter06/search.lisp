(require2 "../utils/utils")
(require2 "../utils/debug")

;; Paths

(defstruct (path (:print-function print-path))
  state (previous nil) (cost-so-far 0) (total-cost 0))

(defun map-path (fn path)
  "Call FN on each state in PATH, collecting results."
  (if (null path)
      nil
      (cons (funcall fn (path-state path))
            (map-path fn (path-previous path)))))

(defun print-path (path &optional (stream t) depth)
  "Print function for path PATH."
  (declare (ignore depth))
  (format stream
          "#<Path to ~a cost ~,1f>"
          (path-state path)
          (path-total-cost path)))


;; Tree Search Functions

(defun tree-search (states goal-p successors combiner)
  "Find a state that satisfies GOAL-P.
Start with STATES and search according to SUCCESSORS and COMBINER."
  (dbg :search "~&:: Search: ~a" states)
  (cond ((null states) fail)
        ((funcall goal-p (first states)) (first states))
        (t (tree-search
            (funcall combiner
                     (funcall successors (first states))
                     (rest states))
            goal-p
            successors
            combiner))))

(defun depth-first-search (start goal-p successors)
  "Search new states first until goal is reached."
  (tree-search (list start) goal-p successors #'append))

(defun breadth-first-search (start goal-p successors)
  "Search old states first until goal is reached."
  (tree-search (list start) goal-p successors #'prepend))

(defun sorter (cost-fn)
  "Return a combiner function that sorts according to COST-FN."
  #'(lambda (new old)
      (sort (append new old) #'< :key cost-fn)))

(defun best-first-search (start goal-p successors cost-fn)
  "Search lowest cost states first until goal is reached."
  (tree-search (list start) goal-p successors (sorter cost-fn)))

(defun beam-search (start goal-p successors cost-fn beam-width)
  "Search highest scoring states first until goal is reached,
but never consider more than BEAM-WIDTH states at a time."
  (tree-search (list start)
               goal-p
               successors
               #'(lambda (old new)
                   (let ((sorted (funcall (sorter cost-fn) old new)))
                     (if (> beam-width (length sorted))
                         sorted
                         (subseq sorted 0 beam-width))))))

(defun iter-wide-search (start goal-p successors cost-fn
                         &key (width 1) (max 100))
  "Search, increasing beam width from WIDTH to MAX.
Return the first solution found at any width."
  (dbg :search ": Width: ~d" width)
  (unless (> width max)
    (or (beam-search start goal-p successors cost-fn width)
        (iter-wide-search start goal-p successors cost-fn
                          :width (+ width 1) :max max))))

;; Graph Search Functions

(defun new-states (states successors state= old-states)
  "Generate successor states that have not been seen before."
  (remove-if
   #'(lambda (state)
       (or (member state states :test state=)
           (member state old-states :test state=)))
   (funcall successors (first states))))

(defun graph-search (states goal-p successors combiner
                     &optional (state= #'eql) old-states)
  "Find a state that satisfies GOAL-P.
Start with STATES and search according to SUCCESSORS and COMBINER.
Don't try the same state twice."
  (dbg :search "~&:: Search: ~a" states)
  (cond ((null states) fail)
        ((funcall goal-p (first states)) (first states))
        (t (graph-search
            (funcall
             combiner
             (new-states states successors state= old-states)
             (rest states))
            goal-p
            successors
            combiner
            state=
            (adjoin (first states) old-states
                    :test state=)))))

;; A* Search

(defun find-path (state paths state=)
  "Find the path with this STATE among a list of PATHS."
  (find state paths :key #'path-state :test state=))

(defun better-path (path1 path2)
  "Is PATH1 cheaper than PATH2?"
  (< (path-total-cost path1) (path-total-cost path2)))

(defun insert-path (path paths)
  "Put PATH into the right position, sorted by total cost."
  (merge 'list (list path) paths #'< :key #'path-total-cost))

(defun path-states (path)
  "Collect the states along this path."
  (if (null path)
      nil
      (cons (path-state path)
            (path-states (path-previous path)))))

(defun a*-search (paths goal-p successors cost-fn cost-left-fn
                  &optional (state= #'eql) old-paths)
  "Find a path whose state satisfies GOAL-P. Start with PATHS,
and expand SUCCESSORS, exploring least cost first.
When there are duplicate states, keep the one with the
lower cost and discard the other."
  (dbg :search ":: Search: ~a" paths)
  (cond ((null paths) fail)
        ((funcall goal-p (path-state (first paths)))
         (values (first paths) paths))
        (t (let* ((path (pop paths))
                  (state (path-state path)))
             ;; Update PATHS and OLD-PATHS to reflect
             ;; the new successors of state
             (setf old-paths (insert-path path old-paths))
             (dolist (state2 (funcall successors state))
               (let* ((cost (+ (path-cost-so-far path)
                               (funcall cost-fn state state2)))
                      (cost2 (funcall cost-left-fn state2))
                      (path2 (make-path
                              :state state2
                              :previous path
                              :total-cost (+ cost cost2)))
                      (old nil))
                 ;; Place the new path, path2, in the right list
                 (cond ((setf old (find-path state2 paths state=))
                        (when (better-path path2 old)
                          (setf paths (insert-path path2 paths))
                          (setf old-paths (delete old old-paths))))
                       (t (setf paths (insert-path path2 paths))))))
             ;; Finally, all A* again with the updated path lists
             (a*-search paths goal-p successors cost-fn cost-left-fn
                        state= old-paths)))))

(defun search-all (start goal-p successors cost-fn beam-width)
  "Find all solutions to a search problem, using `beam-search'."
  (let ((solutions nil))
    (beam-search start
                 #'(lambda (x)
                     (when (funcall goal-p x)
                       (push x solutions))
                     nil)
                 successors
                 cost-fn
                 beam-width)
    solutions))

(provide "search")
