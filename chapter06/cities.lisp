(require2 "./search")

(defconstant earth-diameter 12765.0
  "Diameter of planet earth in kilometers.")

(defstruct (city (:type list)) name long lat)

(defparameter *cities*
  '((Atlanta        84.23 33.45)
    (Los-Angeles   118.15 34.03)
    (Boston         71.05 42.21)
    (Memphis        90.03 35.09)
    (Chicago        87.37 41.50)
    (New-York       73.58 40.47)
    (Denver        105.00 39.45)
    (Oklahoma-City  97.28 35.26)
    (Eugene        123.05  44.03)
    (Pittsburgh     79.57 40.27)
    (Flagstaff     111.41 35.13)
    (Quebec         71.11 46.49)
    (Grand-Jct     108.37 39.05)
    (Reno          119.49 39.30)
    (Houston       105.00 34.00)
    (San-Francisco 122.26 37.47)
    (Indianapolis   86.10 39.46)
    (Tampa          82.27 27.57)
    (Jacksonville   81.40 30.22)
    (Victoria      123.21 48.25)
    (Kansas-City    94.35 39.06)
    (Wilmington     77.57 34.14)))

;; City Functions

(defun deg->radians (deg)
  "Convert degrees and minutes to radians."
  (* (+ (truncate deg) (* (rem deg 1) 100/60)) pi 1/180))

(defun xyz-coords (city)
  "Return the x,y,z coordinates of CITY on a sphere.
The center of the sphere is (0 0 0) and the northern pole is (0 0 1)."
  (let ((psi (deg->radians (city-lat city)))
        (phi (deg->radians (city-long city))))
    (list (* (cos psi) (cos phi))
          (* (cos psi) (sin phi))
          (sin psi))))

(defun distance (point1 point2)
  "Find the Euclidean distance between POINT1 and POINT2.
The points are coordinates in n-dimensional space."
  (sqrt (reduce #'+ (mapcar #'(lambda (a b) (expt (- a b) 2))
                            point1
                            point2))))

(defun air-distance (city1 city2)
  "Find great circle distance between CITY1 and CITY2."
  (let ((d (distance (xyz-coords city1) (xyz-coords city2))))
    (* earth-diameter (asin (/ d 2)))))

(defun neighbors (city)
  "Find all cities within 1000 kilometers of CITY."
  (find-all-if #'(lambda (c)
                   (and (not (eq c city))
                        (< (air-distance c city) 1000.0)))
               *cities*))

(defun city (name)
  "Find the city with this NAME."
  (assoc name *cities*))

(defun is (value &key (key #'identity) (test #'eql))
  "Returns a predicate that tests for a given value."
  #'(lambda (path) (funcall test value (funcall key path))))

(defun path-saver (successors cost-fn cost-left-fn)
  ""
  #'(lambda (old-path)
      (let ((old-state (path-state old-path)))
        (mapcar
         #'(lambda (new-state)
             (let ((old-cost
                     (+ (path-cost-so-far old-path)
                        (funcall cost-fn old-state new-state))))
               (make-path
                :state new-state
                :previous old-path
                :cost-so-far old-cost
                :total-cost (+ old-cost (funcall cost-left-fn
                                                 new-state)))))
         (funcall successors old-state)))))

(defun show-city-path (path &optional (stream t))
  "Show the length of a path, and the cities along it."
  (format stream
          "#<Path ~.1f km: ~{~:(~a~)~^ - ~}>"
          (path-total-cost path)
          (reverse (map-path #'city-name path))))

(defun trip (start dest &optional (beam-width 1))
  "Search for the best path from the START to DEST."
  (beam-search
   (make-path :state start)
   (is dest :key #'path-state)
   (path-saver #'neighbors 
               #'air-distance
               #'(lambda (c) (air-distance c dest)))
   #'path-total-cost
   beam-width))

(provide "cities")
