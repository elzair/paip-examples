(require2 "../utils/utils")
(require2 "../utils/debug")
(require2 "../chapter04/gps-v2")


(defun applicable-ops (state)
  "Return a list of all ops that are applicable for STATE."
  (find-all-if
   #'(lambda (op)
       (subsetp (op-preconds op) state :test #'equal))
   *ops*))

(defun gps-successors (state)
  "Return a list of states reachable from STATE using ops."
  (mapcar
   #'(lambda (op)
       (append
        (remove-if #'(lambda (x)
                       (member-equal x (op-del-list op)))
                   state)
        (op-add-list op)))
   (applicable-ops state)))

(defun search-gps (start goal &optional (beam-width 10))
  "Search for a sequence of operators leading to GOAL."
  (find-all-if
   #'action-p
   (beam-search
    (cons '(start) start)
    #'(lambda (state) (subsetp goal state :test #'equal))
    #'gps-successors
    #'(lambda (state)
        (+ (count-if #'action-p state)
           (count-if #'(lambda (con)
                         (not (member-equal con state)))
                     goal)))
    beam-width)))

(provide "gps-v3")
