(defconstant fail nil
  "Indicates a failure.")

(defun length=1 (x)
  "Is X a list of length 1?"
  (and (consp x) (null (rest x))))

(defun mappend (fn the-list)
  "Apply FN to each element of THE-LIST and append the results."
  (apply #'append (mapcar fn the-list)))

(defun random-elt (set)
  "Return a random element from SET."
  (elt set (random (length set))))

(defun one-of (set)
  "Pick one element of set and make a list of it."
  (list (random-elt set)))

(defun find-all (item sequence &rest keyword-args
                 &key (test #'eql) test-not &allow-other-keys)
  "Find all those elements of a sequence that match item,
according to the keywords. Doesn't alter sequence."
  (if test-not
      (apply #'remove item sequence
             :test-not (complement test-not) keyword-args)
      (apply #'remove item sequence
             :test (complement test) keyword-args)))

(defun member-equal (item list)
  ""
  (member item list :test #'equal))

(defun starts-with (list x)
  "Is this a LIST whose first element is X?"
  (and (consp list) (eql (first list) x)))

(defun find-all-if (fn sequence)
  "Synonym for `remove-if-not`."
  (remove-if-not fn sequence))

(defun mklist (x)
  "Return X if it is a list, otherwise (X)."
  (if (listp x)
      x
      (list x)))

(defun flatten (the-list)
  "Append together elements (or lists) in the list."
  (mappend #'mklist the-list))

(defun compose (f g)
  "Return the function that computes (f (g x))."
  #'(lambda (x) (funcall f (funcall g x))))

(defun prepend (x y)
  "Prepend Y to start of X."
  (append y x))

(defun ^ (x y)
  "Return X raised to the power Y."
  (expt x y))

(provide "utils")
