; helper functions
(defun mappend (fn the-list)
  "Apply FN to each element of THE-LIST and append the results."
  (apply #'append (mapcar fn the-list)))

(defun random-elt (set)
  "Return a random element from SET."
  (elt set (random (length set))))

(defun one-of (set)
  "Pick one element of set and make a list of it."
  (list (random-elt set)))

(defun article ()
  (one-of '(the a)))

(defun noun ()
  (one-of '(man ball woman table)))

(defun verb ()
  (one-of '(hit took saw liked)))

(defun adj ()
  (one-of '(bit little blue green adiabatic)))

(defun prep ()
  (one-of '(to in by with on)))

(defun adj* ()
  (if (= (random 2) 0)
      nil
      (append (adj) (adj*))))

(defun pp ()
  (append (prep) (noun-phrase)))

(defun pp* ()
  (if (random-elt '(t nil))
      (append (pp) (pp*))
      nil))

(defun verb-phrase ()
  (append (verb) (noun-phrase)))

(defun noun-phrase ()
  (append (article) (adj*) (noun) (pp*)))

(defun sentence ()
  (append (noun-phrase) (verb-phrase)))

(defparameter *simple-grammar*
  '((sentence (noun-phrase verb-phrase))
    (noun-phrase (article noun))
    (verb-phrase (verb noun-phrase))
    (article the a)
    (noun man ball woman table)
    (verb ht took saw liked))
  "A grammar for a trivial subset of English.")

(defvar *grammar* *simple-grammar*
  "The grammar used by generate.")

(defun rule-lhs (rule)
  "The left-hand side of RULE."
  (first rule))

(defun rule-rhs (rule)
  "The right-hand side of RULE."
  (rest rule))

(defun rewrites (category)
  "Returns a list of the possible rewrites for this category."
  (rule-rhs (assoc category *grammar*)))

(defun generate (phrase)
  "Generates a random sentence or PHRASE."
  (cond ((listp phrase)
         (mapcan #'generate phrase))
        ((rewrites phrase)
         (generate (random-elt (rewrites phrase))))
        (t (list phrase))))

; Exercise 2.1
(defun generate-2 (phrase)
  "Generates a random sentence or PHRASE without calling `rewrites' twice."
  (let ((phrase-rewrites (rewrites phrase)))
    (cond ((listp phrase)
           (mapcan #'generate-2 phrase))
          (phrase-rewrites
           (generate-2 (random-elt phrase-rewrites)))
          (t (list phrase)))))

; Exercise 2.2
(defun not-terminal-p (category)
  "Check whether or not CATEGORY has rewrite rules."
  (not (null (rewrites category))))

(defun generate-3 (phrase)
  "Generates a random sentence or PHRASE and distinguishes between terminal and nonterminal symbols."
  (cond ((listp phrase)
         (mapcan #'generate-3 phrase))
        ((not-terminal-p phrase)
         (generate-3 (random-elt (rewrites phrase))))
        (t (list phrase))))

(defparameter *bigger-grammar*
  '((sentence (noun-phrase verb-phrase))
    (noun-phrase (article adj* noun pp*) (name) (pronoun))
    (verb-phrase (verb noun-phrase pp*))
    (pp* () (pp pp*))
    (adj* () (adj adj*))
    (pp (prep noun-phrase))
    (prep to in by with on)
    (adj big little blue green adiabatic)
    (article the a)
    (name Pat Kim Lee Terry Robin)
    (noun man ball woman table)
    (verb hit took saw liked)
    (pronoun he she it these those that)))

(setf *grammar* *bigger-grammar*)

(defun generate-tree (phrase)
  "Generate a random sentence or phrase with a complete parse tree."
  (cond ((listp phrase)
         (mapcar #'generate-tree phrase))
        ((rewrites phrase)
         (cons phrase
               (generate-tree (random-elt (rewrites phrase)))))
        (t (list phrase))))

(defun combine-all (xlist ylist)
  "Return a list of lists formed by appending elements from YLIST to elements of XLIST. E.g., (combine-all '((a) (b)) '((1) (2))) -> ((A 1) (B 1) (A 2) (B 2))"
  (mappend #'(lambda (y)
              (mapcar #'(lambda (x)
                          (append x y))
                      xlist))
          ylist))

(defun generate-all (phrase)
  "Generate a list of all possible expansions of this phrase."
  (cond ((null phrase) (list nil))
        ((listp phrase)
         (combine-all (generate-all (first phrase))
                      (generate-all (rest phrase))))
        ((rewrites phrase)
         (mappend #'generate-all (rewrites phrase)))
        (t (list (list phrase)))))

; Exercise 2.4
(defun cross-product (fn xlist ylist)
  "Calculates the cross-product of applying FN to XLIST and YLIST."
  (mappend #'(lambda (y)
                (mapcar #'(lambda (x)
                            (funcall fn x y))
                        xlist))
            ylist))

(defun combine-all-crossed (xlist ylist)
  "Return a list of lists formed by appending elements from YLIST to elements of XLIST. E.g., (combine-all '((a) (b)) '((1) (2))) -> ((A 1) (B 1) (A 2) (B 2))"
  (cross-product #'append xlist ylist))
