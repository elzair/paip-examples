(require2 "./student")

; Exercise 7.1
(defun print-equations (header equations)
  "Print a HEADER followed by a list of EQUATIONS."
  (progn
    (princ header)
    (loop for i in (mapcar #'prefix->infix equations)
          do (progn (terpri)
                    (princ i)))))
