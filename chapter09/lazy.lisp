(defstruct delay (value nil) (function nil))

(defmacro delay (&rest body)
  "A computation that can be executed later by `force'."
  `(make-delay :function #'(lambda () . ,body)))

(defun force (x)
  "Find the value of X by computing if it is a delay."
  (if (not (delay-p x))
      x
      (progn
        (when (delay-function x)
          (setf (delay-value x)
                (funcall (delay-function x)))
          (setf (delay-function x) nil))
        (delay-value x))))

(provide "lazy")
