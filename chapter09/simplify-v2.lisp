(require2 "../chapter08/simplify")
(require2 "./profile")

(defvar *test-data*
  (mapcar
   #'infix->prefix
   '((d (a * x ^ 2 + b * x + c) / d x)
     (d ((a * x ^ 2 + b * x + c) / x) / d x)
     (d ((a * x ^ 3 + b * x ^ 2 + c * x + d) / x ^ 5) / d x)
     ((sin (x + x)) * (sin (2 * x)) + (cos (d (x ^ 2) / d x)) ^ 1)
     (d (3 * x + (cos x) / x) / d x)))
  "Test data for simplify-v2.")

(defvar *answers*
  (mapcar #'simplify *test-data*)
  "Answers to `*test-data`.")

(defun assert-equal (x y)
  "Complain if X is not equal to Y."
  (assert (equal x y)
          (x y)
          "Expected ~a to be equal to ~a" x y))

(defun test-it (&optional (with-profiling t))
  "Time a test run and make sure the answers are correct."
  (let ((answers
          (if with-profiling
              (with-profiling (simplify simplify-exp pat-match
                                        match-variable variable-p)
                (mapcar #'simplify *test-data*))
              (time (mapcar #'simplify *test-data*)))))
    (mapc #'assert-equal answers *answers*)
    t))

(provide "simplify-v2")
