(defun fib (n)
  "Compute the Nth number in the Fibonacci sequence."
  (princ n)
  (if (<= n 1)
      1
      (+ (fib (- n 1))
         (fib (- n 2)))))

(defun memo (fn name key test)
  "Return a memo-function of FN."
  (let ((table (make-hash-table :test test)))
    (setf (get name 'memo) table)
    #'(lambda (&rest args)
        (let ((k (funcall key args)))
          (multiple-value-bind (val found-p)
              (gethash x table)
            (if found-p
                val
                (setf (gethash k table) (apply fn args))))))))

(defun memoize (fn-name &key (key #'first) (test #'eql))
  "Replace FN-NAME's global definition with a memoized version."
  (setf (symbol-function fn-name)
        (memo (symbol-function fn-name) fn-name key test)))

(defun clear-memorize (fn-name)
  "Clear the hash table from a memo function."
  (let ((table (get fn-name 'memo)))
    (when table
      (clrhash table))))

;(defmacro defmemo (fn args &key (key #'first) (test #'eql) &body body)
;  "Define a memoized function."
;  `(progn (defun ,fn ,args . ,body)
;          (memoize ',fn :key ,key :test ,test)))

(provide "memoization")
