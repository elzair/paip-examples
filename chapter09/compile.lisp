(require2 "../utils/utils")

(defun rule-lhs (rule)
  "The left-hand side of a RULE."
  (first rule))

(defun rule-rhs (rule)
  "The right-hand side of a RULE."
  (rest (rest rule)))

(defun build-code (choice)
  "Append together multiple constituents."
  (cond ((null choice) nil)
        ((atom choice) (list choice))
        ((length=1 choice) choice)
        (t `(append ,@(mapcar #'build-code choice)))))

(defun build-cases (number choices)
  "Return a list of case-clauses."
  (when choices
    (cons (list number (build-code (first choices)))
          (build-cases (+ number 1) (rest choices)))))

(defun compile-rule (rule)
  "Translate a grammar RULE into a lisp function definition."
  (let ((rhs (rule-rhs rule)))
    `(defun ,(rule-lhs rule) ()
       ,(cond ((every #'atom rhs) `(one-of ',rhs))
              ((length=1 rhs) (build-code (first rhs)))
              (t `(case (random ,(length rhs))
                    ,@(build-cases 0 rhs)))))))

(defmacro defrule (&rest rule)
  "Define a grammar RULE."
  (compile-rule rule))

(provide "compile")
