(defvar *profiled-functions* nil
  "Function names that are currently profiled.")

(defvar *profile-call-stack* nil "Call stack of profiled function.")

(proclaim '(inline profile-enter profile-exit inc-profile-time))

(defun profile-count (fn-name)
  "Return number of times function FN-NAME was called."
  (get fn-name 'profile-count))

(defun profile-time (fn-name)
  "Return time in profile of function FN-NAME."
  (get fn-name 'profile-time))

(defun get-fast-time ()
  "Return the elapsed time."
  (get-internal-real-time))

(defun fast-time-difference (end start)
  "Return elapsed time from START to END."
  (- end start))

(defun fast-time->seconds (time)
  "Convert a fast-time interval into seconds."
  (/ time internal-time-units-per-second))

(defun inc-profile-time (entry fn-name)
  "Add the new time from ENTRY to the profile of FN-NAME."
  (incf (get fn-name 'profile-time)
        (fast-time-difference (get-fast-time) (cdr entry))))

(defun profile-enter (fn-name)
  "Begin profiling function FN-NAME."
  (incf (get fn-name 'profile-count))
  (unless (null *profile-call-stack*)
    (inc-profile-time (first *profile-call-stack*)  ; Time charged against calling function
                      (car (first *profile-call-stack*))))
  (push (cons fn-name (get-fast-time))              ; Change the top entry to reflect the current time
        *profile-call-stack*))

(defun profile-exit (fn-name)
  "Stop the current profile for function FN-NAME."
  (inc-profile-time (pop *profile-call-stack*)  ; Time charged against the current function
                    fn-name)
  (unless (null *profile-call-stack*)           ; Change the top entry to reflect current time
    (setf (cdr (first *profile-call-stack*))
          (get-fast-time))))

(defun profiled-fn (fn-name fn)
  "Return a function that increments the count."
  #'(lambda (&rest args)
      (incf (get fn-name 'profile-count))
      (apply fn args)))

(defun profile1 (fn-name)
  "Make the function count how often it is called."
  (let ((fn (symbol-function fn-name)))
    (unless (eq fn (get fn-name 'profiled-fn))
      (let ((new-fn (profiled-fn fn-name fn)))
        (setf (symbol-function fn-name) new-fn
              (get fn-name 'unprofiled-fn) fn
              (get fn-name 'profile-time) 0
              (get fn-name 'profile-count) 0))))
  fn-name)

(defmacro profile (&rest fn-names)
  "Profile FN-NAMES.With no args, list profiled functions."
  `(mapcar #'profile1
           (setf *profiled-functions*
                 (union *profiled-functions* ',fn-names))))

(defun unprofile1 (fn-name)
  "Make the function stop counting how often it is called."
  (setf (get fn-name 'profile-time) 0)
  (setf (get fn-name 'profile-count) 0)
  (when (eq (symbol-function fn-name) (get fn-name 'profiled-fn))
    (setf (symbol-function fn-name)
          (get fn-name 'unprofiled-fn)))
  fn-name)

(defmacro unprofile (&rest fn-names)
  "Stop profiling FN-NAMES. With no args, stop all profiling."
  `(progn
     (mapcar #'unprofile1
             ,(if fn-names `',fn-names '*profiled-functions*))
     (setf *profiled-functions*
           ,(if (null fn-names)
                nil
                `(set-difference *profiled-functions*
                                 ',fn-names)))))

(defun profile-report (&optional
                         (fn-names (copy-list *profiled-functions*))
                         (key #'profile-count))
  "Report profiling statistics on given functions."
  (let ((total-time (reduce #'+ (mapcar #'profile-time fn-names))))
    (unless (null key)
      (setf fn-names (sort fn-names #'> :key key)))
    (format t "~&Total elapsed time: ~d seconds."
            (fast-time->seconds total-time))
    (format t "~& Count  Secs Time% Name")
    (loop for name in fn-names do
      (format t "~&~7D ~6,2F  ~3d% ~A"
              (profile-count name)
              (fast-time->seconds (profile-time name))
              (round (/ (profile-time name) total-time) .01)
              name))))

(defmacro with-profiling (fn-names &rest body)
  "Enable profiling on the functions in FN-NAMES."
  `(progn
     (unprofile . ,fn-names)
     (profile . ,fn-names)
     (setf *profile-call-stack* nil)
     (unwind-protect
          (progn . ,body)
       (profile-report ',fn-names)
       (unprofile . ,fn-names))))

(provide "profile")
