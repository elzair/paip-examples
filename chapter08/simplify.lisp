(require2 "../chapter06/pat-match")
(require2 "./helpers")
(require2 "./rules")

(defun variable-p (exp)
  "Variables are the symbols M through Z."
  (member exp '(x y z m n o p q r s t u v w)))

(defun not-numberp (x)
  "Return true if X is not a number."
  (not (numberp x)))


(defun evaluable (exp)
  "Is EXP an arithmetic expression that can be evaluated?"
  (and (every #'numberp (exp-args exp))
       (or (member (exp-op exp) '(+ - * /))
           (and (eq (exp-op exp) '^)
                (integerp (second (exp-args exp)))))))

(defun simp-fn (op)
  "Retrieve simplification function for operator OP."
  (get op 'simp-fn))

(defun set-simp-fn (op fn)
  "Set simplification function for operator OP to FN."
  (setf (get op 'simp-fn) fn))

(defun simplify-by-fn (exp)
  "If there is a simplification function for EXP,
and if applying it gives a non-null result,
then simplify the result and return that."
  (let* ((fn (simp-fn (exp-op exp)))
         (result (if fn (funcall fn exp))))
    (if (null result)
        nil
        (simplify result))))

(defun simplify-exp (exp)
  "Simplify EXP by either using a rule or doing arithmetic."
  (cond ((simplify-by-fn exp))
        ((rule-based-translator
          exp
          *simplification-rules*
          :rule-if #'exp-lhs
          :rule-then #'exp-rhs
          :action #'(lambda (bindings response)
                      (simplify (sublis bindings
                                        response)))))
        ((evaluable exp) (eval exp))
        (t exp)))

(defun simplify (exp)
  "Simplify expression EXP by first simplifying its components."
  (if (atom exp)
      exp
      (simplify-exp (mapcar #'simplify exp))))

(defun simplify-input (inf)
  "Simplify inputted infix expression INF."
  (prefix->infix (simplify (infix->prefix inf))))

(defun simplifier ()
  "Read a mathematical expression, simplify it, and print the result."
  (loop
        (print 'simplifier>)
    (print (simplify-input (read)))))

(set-simp-fn 'Int
             #'(lambda (exp)
                 (unfactorize
                  (factorize
                   (integrate (exp-lhs exp) (exp-rhs exp))))))

(provide "simplify")
