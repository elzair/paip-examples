(require2 "../utils/utils")
(require2 "../chapter06/pat-match")

(defstruct (rule (:type list)) pattern response)

(defstruct (exp (:type list)
                (:constructor mkexp (lhs op rhs)))
  op lhs rhs)

(defun exp-args (x)
  "Return arguments of expression X."
  (rest x))

(defun exp-p (x)
  "Return true if X is an exp."
  (consp x))

(defun binary-exp-p (x)
  "Return true if X is an exp of length 2."
  (and (exp-p x)
       (= (length (exp-args x)) 2)))

(defun prefix->infix (exp)
  "Translate EXP from prefix to infix notation."
  (if (atom exp)
      exp
      (mapcar #'prefix->infix
              (if (binary-exp-p exp)
                  (list (exp-lhs exp)
                        (exp-op exp)
                        (exp-rhs exp))
                  exp))))

(defun infix->prefix (infix-exp)
  "Convert fully parenthesized INFIX-EXP to a prefix expression."
  (cond ((atom infix-exp) infix-exp)
        ((= (length infix-exp) 1) (infix->prefix (first infix-exp)))
        ((rule-based-translator infix-exp
                                *infix->prefix-rules*
                                :rule-if #'rule-pattern
                                :rule-then #'rule-response
                                :action
                                #'(lambda (bindings response)
                                    (sublis (mapcar
                                             #'(lambda (pair)
                                                 (cons (first pair)
                                                       (infix->prefix (rest pair))))
                                             bindings)
                                            response))))
        ((symbolp (first infix-exp))
         (list (first infix-exp) (infix->prefix (rest infix-exp))))
        (t (error "Illegal exp"))))

(defun simp-rule (rule)
  "Transform RULE into proper format."
  (let ((exp (infix->prefix rule)))
    (mkexp (expand-pat-match-abbrev (exp-lhs exp))
           (exp-op exp)
           (exp-rhs exp))))

(provide "simplify-helpers")
