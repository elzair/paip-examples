(require2 "./helpers")

(defun integration-table (rules)
  "Create integration table from RULES."
  (dolist (i-rule rules)
    (let ((rule (infix->prefix i-rule)))
      (setf (get (exp-op (exp-lhs (exp-lhs rule))) 'int)
            rule))))

(defun in-integral-table? (exp)
  "Return true if EXP is in the integral table."
  (and (exp-p exp) (get (exp-op exp) 'int)))

(defun integrate-from-table (op arg)
  "Return result of substitution of ARG by the rule specified for OP."
  (let ((rule (get op 'int)))
    (subst arg (exp-lhs (exp-lhs (exp-lhs rule))) (exp-rhs rule))))

(defun find-anywhere (item tree)
  "Does ITEM occur anywhere in TREE?  If so, return it."
  (cond ((eql item tree) tree)
        ((atom tree) nil)
        ((find-anywhere item (first tree)))
        ((find-anywhere item (rest tree)))))

(defun free-of (exp var)
  "True if expression EXP has no occurrence of VAR."
  (not (find-anywhere var exp)))

(defun factorize (exp)
  "Return a list of the factors of EXP^n,
where each factor is of the form (^ y n)."
  (let ((factors nil)
        (constant 1))
    (labels
        ((fac (x n)
           (cond ((numberp x)
                  (setf constant (* constant (expt x n))))
                 ((starts-with x '*)
                  (fac (exp-lhs x) n)
                  (fac (exp-rhs x) n))
                 ((starts-with x '/)
                  (fac (exp-lhs x) n)
                  (fac (exp-rhs x) (- n)))
                 ((and (starts-with x '-)
                       (length=1 (exp-args x)))
                  (setf constant (- constant))
                  (fac (exp-lhs x) n))
                 ((and (starts-with x '^)
                       (numberp (exp-rhs x)))
                  (fac (exp-lhs x) (* n (exp-rhs x))))
                 (t (let ((factor (find x factors
                                        :key #'exp-lhs
                                        :test #'equal)))
                      (if factor
                          (incf (exp-rhs factor) n)
                          (push `(^ ,x ,n) factors)))))))
      (fac exp 1)
      (case constant
        (0 '((^ 0 1)))
        (1 factors)
        (t `((^ ,constant 1) .,factors))))))

(defun unfactorize (factors)
  "Convert a list of FACTORS back into prefix form."
  (cond ((null factors) 1)
        ((length=1 factors) (first factors))
        (t `(* ,(first factors) ,(unfactorize (rest factors))))))

(defun divide-factors (numer denom)
  "Return a list of the elements in list NUMER divided by the elements in list DENOM."
  (let ((result (mapcar #'copy-list numer)))
    (dolist (d denom)
      (let ((factor (find (exp-lhs d)
                          result
                          :key #'exp-lhs
                          :test #'equal)))
        (if factor
            (decf (exp-rhs factor) (exp-rhs d))
            (push `(^ ,(exp-lhs d) ,(- (exp-rhs d))) result))))
    (delete 0 result :key #'exp-rhs)))

(defun deriv (y x)
  "Find the derivative of Y with respect to X."
  (simplify `(d ,y ,x)))

(defun deriv-divides (factor factors x)
  "Find deriv-divides."
  (assert (starts-with factor '^))
  (let* ((u (exp-lhs factor))              ; factor = u^n
         (n (exp-rhs factor))
         (k (divide-factors 
              factors (factorize `(* ,factor ,(deriv u x))))))
    (cond ((free-of k x)
           ;; Int k*u^n*du/dx dx = k*Int u^n du
           ;;                    = k*u^(n+1)/(n+1) for n/=1
           ;;                    = k*log(u) for n=1
           (if (= n -1)
               `(* ,(unfactorize k) (log ,u))
               `(/ (* ,(unfactorize k) (^ ,u ,(+ n 1)))
                   ,(+ n 1))))
          ((and (= n 1) (in-integral-table? u))
           (let ((k2 (divide-factors       ; Int y'*f(y) dx = Int f(y) dy
                       factors
                       (factorize `(* ,u ,(deriv (exp-lhs u) x))))))
             (if (free-of k2 x)
                 `(* ,(integrate-from-table (exp-op u) (exp-lhs u))
                     ,(unfactorize k2))))))))

(defun partition-if (pred list)
  "Return 2 values: elements of list that satisfy pred,
  and elements that don't."
  (let ((yes-list nil)
        (no-list nil))
    (dolist (item list)
      (if (funcall pred item)
          (push item yes-list)
          (push item no-list)))
    (values (nreverse yes-list) (nreverse no-list))))

(defun integrate (exp x)
  (cond                                             ; First try some trivial cases
    ((free-of exp x) `(* ,exp x))                   ; Int c dx = c*x
    ((starts-with exp '+)                           ; Int f + g  = 
     `(+ ,(integrate (exp-lhs exp) x)               ;   Int f + Int g
         ,(integrate (exp-rhs exp) x)))
    ((starts-with exp '-)              
     (ecase (length (exp-args exp))                 
       (1 (integrate (exp-lhs exp) x))              ; Int - f = - Int f
       (2 `(- ,(integrate (exp-lhs exp) x)          ; Int f - g  =
              ,(integrate (exp-rhs exp) x)))))      ; Int f - Int g
    ((multiple-value-bind (const-factors x-factors) ; Now move the constant factors to the left of the integral
         (partition-if #'(lambda (factor) (free-of factor x))
                       (factorize exp))
       (identity                                    ; simplify
        `(* ,(unfactorize const-factors)
            ,(cond ((null x-factors) x)             ; Try to integrate
                   ((some #'(lambda (factor)
                              (deriv-divides factor x-factors x))
                          x-factors))
                   ;; <other methods here>
                   (t `(int? ,(unfactorize x-factors) ,x)))))))))

(provide "simplify-integration")
