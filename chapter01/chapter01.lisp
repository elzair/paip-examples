; Exercise 1.1
(defparameter *titles*
           '(Mr Mrs Miss Ms Sir Madam Dr Admiral Major General MD)
           "A list of titles that can appear at the start of a name.")

(defun last-name (name)
  "Select the first name from a NAME represented as a list."
  (if (member (first (last name)) *titles*)
      (last-name (reverse (rest (reverse name))))
      (first (last name))))

; Exercise 1.2
(defun power (x y)
  "Return X raised to power Y."
  (if (= y 0)
      1
      (* x (power x (- y 1)))))

; Exercise 1.3
(defun count-atoms (exp)
  "Return the total number of non-nil atoms in EXP."
  (cond ((null exp) 0)
        ((atom exp) 1)
        (t (+ (count-atoms (first exp))
              (count-atoms (rest exp))))))

(defun count-all-atoms (exp &optional (if-null 1))
  (cond ((null exp) if-null)
        ((atom exp) 1)
        (t (+ (count-all-atoms (first exp) 1)
              (count-all-atoms (rest exp) 0)))))

; Exercise 1.4
(defun count-anywhere (expa expb)
  "Return the number of times EXPA appears in EXPB."
  (cond ((eql expa expb) 1)
        ((atom expb) 0)
        (t (count-anywhere expa (first expb))
           (count-anywhere expa (rest expb)))))

; Exercise 1.5
(defun dot (a b)
  "Return the dot product of list A and list B."
  (if (or (null a) (null b))
      0
      (+ (* (first a) (first b))
         (dot (rest a) (rest b)))))
